-- (a) Find all artist that has letter d in its name
SELECT * FROM artists WHERE name LIKE '%d%';

-- (b) Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- (c) Join the 'albums' and 'songs' tables. (Only show the album name, song name and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON songs.album_id = albums.id;

-- (d) Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name).
SELECT * FROM albums
    JOIN artists ON albums.artist_id = artists.id
    WHERE albums.album_title LIKE '%a%';

-- (e) Sort the albums in Z-A order. (Swow only the first 4 records)
SELECT * from albums ORDER BY album_title DESC LIMIT 4;

-- (f) Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums
    JOIN songs ON songs.album_id = albums.id
    ORDER BY albums.album_title DESC;

    